import { Text, View, TouchableOpacity, Image } from 'react-native';
import {styles} from './styles';
import React from 'react';
import { useNavigation } from '@react-navigation/native';


type Props = {
  showText?: boolean;
}

export function Header({ showText = false }: Props) {
  const navigation = useNavigation();

  function handleGoBack(){
    navigation.navigate("Groups");
  }

  return (
    <View style={styles.container}>
      {
        showText &&
        <TouchableOpacity onPress={handleGoBack}><Image source={require('../../assets/seta.png')}/></TouchableOpacity>
      }
      
      <Image style={styles.logo} source={require('../../assets/logo.png')}/> 
    </View>
  );
}

