import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        width: 350,
        height: 56,
        backgroundColor: 'red',
        borderRadius: 6,
        justifyContent: 'center',
        alignSelf: 'center',
        alignItems: 'center',
    },

    titulo: {
        fontSize: 16,
        color: 'white',
        fontWeight: '700',
    },

})