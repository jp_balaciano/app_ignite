import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: 350,
        height: 90,
        backgroundColor: '#323238',
        fontWeight: '500',
        borderRadius: 6,
        flexDirection: 'row',
        alignItems:'center',
        alignSelf: 'center',
        marginBottom: 12,
    },

    titulo: {
        fontSize: 16,
        color: 'white',
        fontWeight: '200',
    },

    icone: {
        width: 40,
        height: 40,
        color: 'green',
        fontWeight: '700',
        marginLeft: 15,
        marginRight: 20
    }

    
})