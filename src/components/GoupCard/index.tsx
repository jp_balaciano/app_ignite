import { Text, View, Image, TouchableOpacityProps } from 'react-native';
import {styles} from './styles';
import React from 'react';

type Props = TouchableOpacityProps &  {
    title: string;

}


export function GroupCard({ title, ...rest }: Props) {
  return (
    <View style={styles.container} {...rest}>
        <Image style={styles.icone} source={require('../../assets/membros.png')}/>
        <Text style={styles.titulo}> {title} </Text>
    </View>
  );
}

