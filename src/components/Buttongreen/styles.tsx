import { StyleSheet } from "react-native";


export const styles = StyleSheet.create({
    container: {
        width: 350,
        height: 56,
        backgroundColor: 'green',
        borderRadius: 6,
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        
    },

    titulo: {
        fontSize: 16,
        color: 'white',
        fontWeight: '700',
    },

})