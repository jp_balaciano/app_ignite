import { Text, View, Image, TouchableOpacityProps } from 'react-native';
import { styles } from './styles';
import React from 'react';

type Props = TouchableOpacityProps & {
  title: string;
}

export function Button_Green({title, ...rest}: Props) {
  return (
    <View style={styles.container} {...rest}>
        <Text style={styles.titulo}>{title}</Text>
    </View>
  );
}

