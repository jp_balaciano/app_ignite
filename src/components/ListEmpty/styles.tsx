import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    message: {
        textAlign: 'center',
        fontSize: 14,
        color: '#7c7c8a'
    },
})