import { Text, View, Image,  } from 'react-native';
import {styles} from './styles';
import React from 'react';

type Props = {
    message: string;
}

export function ListEmpty(  {message} : Props) {
  return (
    <View style={styles.container}>
        <Text style={styles.message}> {message} </Text>
    </View>
  );
}

