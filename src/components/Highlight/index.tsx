import { Text, View } from 'react-native';
import {styles} from './styles';
import React from 'react';

type Props = {
    title: string;
    subtitle: string;
}


export function Highlight({title, subtitle } : Props) {
  return (
    <View style={styles.container}>
        <Text style={styles.titulo}> {title} </Text>
        <Text style={styles.subtitulo}> {subtitle} </Text>
    </View>
  );
}

