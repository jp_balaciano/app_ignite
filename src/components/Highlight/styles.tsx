import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginTop: 32,
        marginBottom: 32,
        gap: '5',
    },
  
    titulo: {
        textAlign: 'center',
        fontSize: 24,
        color: 'white'
    },

    subtitulo: {
        textAlign: 'center',
        fontSize: 16,
        color: 'gray',
        fontWeight: '300'
    }

})