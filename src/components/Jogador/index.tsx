import { Text, View, Image, TouchableOpacity } from 'react-native';
import { styles } from './styles'; 
import React from 'react';

type Props = {
    title: string;
    onRemove: () => void;
}


export function Jogador({ title, onRemove }: Props) {
  return (
    <View style={styles.container}>
      <View style={styles.dados1}>
        <Image style={styles.pessoa} source={require('../../assets/person.png')}/>
        <Text style={styles.titulo}> {title} </Text>
      </View>

      <TouchableOpacity onPress={onRemove}><Image source={require('../../assets/excluir.png')}/></TouchableOpacity>
    </View>
  );
}

