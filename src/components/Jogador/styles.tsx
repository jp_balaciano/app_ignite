import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        width: 350,
        height: 54,
        backgroundColor: '#323238',
        fontWeight: '500',
        borderRadius: 6,
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10,
        alignItems:'center',
        marginBottom: 15
    },

    dados1: {
        flexDirection: 'row',
        alignItems: 'center',
        gap: 10
    },

    pessoa: {
        width: 27,
        height: 24,
    },

    titulo: {
        fontSize: 16,
        color: '#C4C4CC',
        fontWeight: '300',
    },

    
})