import { Text, View, Image, Touchable, TouchableOpacity } from 'react-native';
import { styles } from './styles';
import React from 'react';

type FilterProps =  {
  title: string;
}

export function Filter({title}: FilterProps) {
  return (
    <View style={styles.container}>
      <TouchableOpacity><Text style={styles.titulo}>{title}</Text></TouchableOpacity>
    </View>
  );
}

