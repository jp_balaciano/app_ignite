
import { View, Image, TextInput, TouchableOpacity, Alert } from 'react-native';
import { styles } from './styles';

import { Header } from '../../components/Header';
import { Highlight } from '../../components/Highlight';
import { Button_Green } from '../../components/Buttongreen';
import { useNavigation } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useState } from 'react';
import { groupCreate } from '../../storage/group/groupCreate';
import { AppError } from '../../utils/AppError';
import { Keyboard } from 'react-native';


export function NewGroup(props) {
    const [group, setGroup] = useState(' ');

    const navigation = useNavigation();

    async function handleNew(){
        try{
            if(group.trim().length === 0){ //Trim ignora os espaços e dessa forma não deixa botar uma turma sem nome
                return Alert.alert('Novo Grupo', 'Informe o nome da turma');
            }

            await groupCreate(group)
            props.navigation.navigate('Players', { group });
            Keyboard.dismiss();

        }catch(error){ //exceção
            if(error instanceof AppError) {
                Alert.alert('Novo Grupo', error.message);
            }else{
                console.log(error);
                Alert.alert('Novo Grupo', 'Não foi possível criar um novo grupo');
            }
            

        }
        
    }


  return (
    <SafeAreaView style={styles.container}>
        <Header showText/>
        <View style={styles.content}>
            <Image style={styles.membros} source={require('../../assets/membros2.png')}/>

            <Highlight 
                title='Nova Turma'
                subtitle='crie uma turma par adicionar pessoas'
            />

            <View style={styles.caixa}>
                <TextInput style={styles.text}
                    placeholder='Nome da Turma'
                    placeholderTextColor="black"
                    onChangeText={setGroup}
                    
                /> 
            </View>

        </View>
        <TouchableOpacity onPress={handleNew}>
            <Button_Green 
                title='Criar'
            />
        </TouchableOpacity>
    </SafeAreaView>

  );
}

