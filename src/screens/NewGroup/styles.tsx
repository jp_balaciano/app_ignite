import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        height: '100%',
        width: '100%',
    },

    content: {
        marginTop: 150,
    },

    membros: {
        width: 56,
        height: 56,
        alignSelf: 'center'
    },

    caixa: {
        backgroundColor: 'gray',
        borderRadius: 6,
        width: 350,
        height: 56,
        alignSelf: 'center',
        marginBottom: 20,
        justifyContent: 'center',
    },

    text: {
        color: 'white',
        fontWeight: '300',
        marginLeft: 20,
        fontSize: 16
    },

})