import { useState, useCallback } from 'react';
import {  FlatList } from 'react-native';
import { useNavigation, useFocusEffect } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';

import { TouchableOpacity, View } from 'react-native';
import {styles} from './style';
import { Header } from '../../components/Header';
import { Highlight } from '../../components/Highlight';
import { GroupCard } from '../../components/GoupCard';
import { ListEmpty } from '../../components/ListEmpty';
import { Button_Red } from '../../components/Buttonred';
import { Button_Green } from '../../components/Buttongreen';
import { groupsGetAll } from '../../storage/group/groupsGetAll';


export function Groups(props) {
  const [groups, setGroups] = useState<string[]>([]);

  const navigation = useNavigation();

  function handleNewGroup() {
    props.navigation.navigate('New');
  }

  async function fetchGroups(){
    try{
      const data = await groupsGetAll();
      setGroups(data);
    }catch(error){
      console.log(error);
    }
  }

  function handleOpenGroup(group: string){
    props.navigation.navigate('Players', { group });
  }

  useFocusEffect(useCallback(() => {
    fetchGroups();
  }, []));

  return (
    <>
    <SafeAreaView style={styles.container}>
      <Header />

      <Highlight 
        title='Turmas'
        subtitle='Jogue com a sua turma'
      />

      <FlatList 
        data={groups}
        keyExtractor={item => item}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => handleOpenGroup(item)}>
            <GroupCard 
              title={item}
            />
          </TouchableOpacity>
          //O onPress={() => handleOpenGroup(item)} vai levar o que está escrito no item para a próxima página (O ideal seria não ter o touchableopacity e o onpress estar dentro do Group´Card, mas não está funcionando)
        )}
        contentContainerStyle={groups.length === 0 && {marginTop: 200}}
        ListEmptyComponent={() => <ListEmpty message="Não tem nenhuma turma cadastrada" />}
      />

      <TouchableOpacity onPress={handleNewGroup}>
        <Button_Green
          title='Criar nova turma'
        />
      </TouchableOpacity>
       
    </SafeAreaView>
    </>
  );
}

