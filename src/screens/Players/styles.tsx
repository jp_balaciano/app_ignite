import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
    container: {
        backgroundColor: 'black',
        height: '100%',
        width: '100%',

    },

    content: {
        alignItems: 'center',
        gap: 20
    },

    caixa: {
        backgroundColor: 'gray',
        borderRadius: 6,
        width: 350,
        height: 56,
        alignSelf: 'center',
        marginBottom: 20,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        padding: 5
    },

    text: {
        color: 'white',
        fontWeight: '300',
        fontSize: 16
    },

    headerlist: {
        width: '100%',
        flexDirection: 'row',
        marginLeft: 50
    },

    quantidade: {
        fontSize: 14,
        color: 'white',
        marginRight: 50
    },


})