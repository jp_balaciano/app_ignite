import { Alert, FlatList, TouchableOpacity } from 'react-native';
import { View, Image, TextInput, Text, ScrollView } from 'react-native';
import { styles } from './styles';
import { Keyboard } from 'react-native';
import { Header } from '../../components/Header';
import { Highlight } from '../../components/Highlight';
import { Jogador } from '../../components/Jogador';
import { Filter } from '../../components/Filter';
import { useState, useEffect } from 'react';
import { ListEmpty } from '../../components/ListEmpty';
import { Button_Red } from '../../components/Buttonred';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useRoute } from '@react-navigation/native';
import { AppError } from '../../utils/AppError';
import { playerAddByGroup } from '../../storage/player/playerAddByGroup';
import { playersGetByGroup } from '../../storage/player/playersGetByGroup';
import { playersGetByGroupAndTeam } from '../../storage/player/playersGetByGroupAndTeams';
import { PlayerStorageDTO } from '../../storage/player/PlayerStorageDTO';
import { playerRemoveByGroup } from '../../storage/player/playerRemoveByGroup';
import { groupRemoveByName } from '../../storage/group/groupRemoveByName';
import { useNavigation } from '@react-navigation/native';

type RouteParams = {
    group: string;
}


export function Players() {
    const[team, setTeam] = useState('Time A');
    const[players, setPlayers] = useState<PlayerStorageDTO[]>([]);
    const [newPlayerName, setNewPlayerName] = useState("");

    const route = useRoute();
    const { group } = route.params as RouteParams;

    const navigation = useNavigation();

    async function handleAddPlayer(){
        if(newPlayerName.trim().length===0){
            return Alert.alert("Nova pessoa", "Informe o nome da pessoa para adicionar");
        }

        const newPlayer = {
            name: newPlayerName,
            team,
        }

        try {
            await playerAddByGroup(newPlayer, group);

            Keyboard.dismiss();

            setNewPlayerName("");
            fetchPlayersByTeam();

        } catch (error) {
            if(error instanceof AppError){
                Alert.alert("Nova pessoa", error.message)
            }else{
                Alert.alert("Nova Pessoa", "Não foi possível adicionar");
                console.log(error);
            }
        }
    }

    async function fetchPlayersByTeam(){
        try{
            const playersByTeam = await playersGetByGroupAndTeam(group, team);
            setPlayers(playersByTeam);
        }catch(error){
            Alert.alert("Pessoas", "Não foi possível carregar as pessoas do time selecionado")
        }
    }

    async function handlePlayerRemove(playerName:string) {
        try {
            await playerRemoveByGroup(playerName, group);
            fetchPlayersByTeam();

        } catch (error) {
            Alert.alert("Remover Pessoa", "Não foi possível remover essa  pessoas");
        }
    }

    async function groupRemove(){
        try {
            await groupRemoveByName(group);
            navigation.navigate("Groups");

        } catch (error) {
            Alert.alert("Remover grupo", "Não foi possível remover o grupo");
        }
    }

    async function handleGroupRemove(){
        Alert.alert(
            "Remover",
            "Deseja remover o grupo?",
            [
                { text: "Não", style: 'cancel'},
                { text: "Sim", onPress: () => groupRemove()}
            ]
        );
    }

    useEffect(() => {
        fetchPlayersByTeam();
    }, [team]);

  return (
    <SafeAreaView style={styles.container}>
        <Header showText/>
        
        <ScrollView showsVerticalScrollIndicator={false}>
        <View  style={styles.content}>
            <Highlight 
                title={group}
                subtitle='adicione a galera e separe os times'
            />


            <View style={styles.caixa}>
                <TextInput style={styles.text}
                    placeholder='Nome da pessoa'
                    placeholderTextColor="black"
                    autoCorrect={false}
                    onChangeText={setNewPlayerName}
                    value={newPlayerName}
                />

                <TouchableOpacity onPress={handleAddPlayer}><Image source={require("../../assets/plus.png")}/></TouchableOpacity>
            </View>

            <View style={styles.headerlist}>
                <FlatList 
                    data={['Time A', 'Time B']}
                    keyExtractor={item => item}
                    renderItem={({item}) => (
                        <Filter 
                            title={item}
                            onPress={() => setTeam(item)}
                        />
                    )}
                    horizontal
                />

                <Text style={styles.quantidade}>
                    {players.length}
                </Text>
            </View>



            <FlatList 
                data={players}
                keyExtractor={item => item.name}
                renderItem={({item}) => (
                    <Jogador 
                        title={item.name}
                        onRemove={() => handlePlayerRemove(item.name)}
                    />
                )}
                ListEmptyComponent={() => (
                    <ListEmpty 
                        message='Não há pessoas nesse time'
                    />
                )}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{flex: 1}}
            />

        </View>
        </ScrollView>

        <TouchableOpacity onPress={handleGroupRemove}>
            <Button_Red 
                title='Remover Turma'
            />
        </TouchableOpacity>
       
    </SafeAreaView>

  );
}

