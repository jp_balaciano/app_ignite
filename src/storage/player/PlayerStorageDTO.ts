//Tipagem para dizer o que a gente fala sobre os jogadores

export type PlayerStorageDTO = {
    name: string;
    team: string;
}