const GROUP_COLLECTION = "@ignite-teams: groups";   //Essa é a chave para poder passar para o AsyncStorage para o Group

const PLAYER_COLLECTION = "@ignite-teams: players";   //Essa é a chave para poder passar para o AsyncStorage para o Player

export { GROUP_COLLECTION, PLAYER_COLLECTION }; 