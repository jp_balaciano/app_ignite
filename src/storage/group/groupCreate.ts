import AsyncStorage from "@react-native-async-storage/async-storage"; //Para podermos usar
import { GROUP_COLLECTION } from "../storageConfig";  //Importamos a chave
import { groupsGetAll } from "./groupsGetAll";
import { AppError } from "../../utils/AppError";

export async function groupCreate(newGroup: string){  
    try{
        const storagedGroups = await groupsGetAll();

        //Se escrevermos um grupo que já existe
        const groupAlreadyExists = storagedGroups.includes(newGroup);

        if(groupAlreadyExists) {
            throw new AppError('Já existe um grupo cadastrado com esse nome.');
        }
        //Para a verificação do grupo já existente


        const storage = JSON.stringify( [...storagedGroups, newGroup]); 

        await AsyncStorage.setItem(GROUP_COLLECTION, storage);
    }catch(error){
        throw error; 
    }
}