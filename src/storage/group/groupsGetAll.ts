import AsyncStorage from "@react-native-async-storage/async-storage";
import { GROUP_COLLECTION } from "../storageConfig";

//Esse arquivo vai servir para listar todas as turmas cadastradas

export async function groupsGetAll(){
    try {
        const storage = await AsyncStorage.getItem(GROUP_COLLECTION); //Quando queremos pegar os dados do dispositivo

        //Processo contrário, texto para objeto

        const groups: string[] = storage ? JSON.parse(storage) : []; //Verifica se tem conteúdo, se não tiver, devolve um array vazio.

        return groups;
    }catch(error) {
        throw error;
    }
    
}